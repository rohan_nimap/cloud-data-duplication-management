from django.urls import path
from .views import *

urlpatterns = [
    path('upload/new/', FileUploadView.as_view()),
    path('uploads/', uploadedFiles.as_view())
]